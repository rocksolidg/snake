
function getRandomPosition (fieldSize) {
  return { x: Math.floor(Math.random() * fieldSize.width), y: Math.floor(Math.random() * fieldSize.height) }
}

function pointsAreEqual (point1, point2) {
  return (point1.x === point2.x && point1.y === point2.y)
}

function getVWandVH (width, height, unit) {
  const ret = { w: width, h: height }

  if (unit === 'c') { // custom
    ret.w += 'vw'
    ret.h += 'vh'
  } else if (unit === 'w') {
    ret.w += 'vw'
    ret.h += 'vw'
  } else if (unit === 'h') {
    ret.w += 'vh'
    ret.h += 'vh'
  }

  return ret
}

function getFieldSize (heightLimitInWH, widthLimitInWW, fieldSize, maxHeightFromFieldInPx, maxWidthFromFieldInPx) {
  let ret = { w: 0, h: 0 }

  // Bessere Namen geben; zsmhänge herausfinden
  const e1 = getVWandVH(widthLimitInWW, widthLimitInWW * fieldSize.height / fieldSize.width, 'w')
  const e2 = getVWandVH(widthLimitInWW, heightLimitInWH, 'c')
  const e3 = getVWandVH(fieldSize.width / fieldSize.height * heightLimitInWH, heightLimitInWH, 'h') // einmal
  const e4 = getVWandVH(widthLimitInWW, widthLimitInWW, 'w') // einmal
  const e5 = getVWandVH(heightLimitInWH * fieldSize.width / fieldSize.height, heightLimitInWH, 'h')
  const e6 = getVWandVH(heightLimitInWH, heightLimitInWH, 'h') // einmal

  if (maxHeightFromFieldInPx === maxWidthFromFieldInPx) { // verfügbare Fläche für das Feld ist ein quadrat
    if (fieldSize.width === fieldSize.height) ret = e2 // Snake Feld ist ein Quadrat
    else if (fieldSize.width > fieldSize.height) ret = e1 // Snake Feld ist ein Rechteck (waagerecht)
    else ret = e3 // Snake Feld ist ein Rechteck (hochkant)
  } else if (maxHeightFromFieldInPx > maxWidthFromFieldInPx) { // verfügbare Fläche für das Feld ist ein rechteck (hochkant)
    if (fieldSize.width === fieldSize.height) ret = e4 // Snake Feld ist ein Quadrat
    else if (fieldSize.width > fieldSize.height) ret = e1 // Snake Feld ist ein Rechteck (waagerecht)
    else { // Snake Feld ist ein Rechteck (hochkant)
      if (fieldSize.width / fieldSize.height === maxWidthFromFieldInPx / maxHeightFromFieldInPx) ret = e2 // Snake Felt hat daselbe seitenverhältnis wie das verfügbare Feld
      else if (fieldSize.width / fieldSize.height /* ]0;1[ */ > maxWidthFromFieldInPx / maxHeightFromFieldInPx /* ]0;1[ */) ret = e1 // Snake Felt stößt zuerst links/rechts an => width = verfügbare Breite
      else ret = e5 // Snake Felt stößt zuerst oben/unten an
    }
  } else { // verfügbare Fläche für das Feld ist ein rechteck (waagerecht)
    if (fieldSize.width === fieldSize.height) ret = e6 // Snake Feld ist ein Quadrat
    else if (fieldSize.width > fieldSize.height) { // Snake Feld ist ein Rechteck (waagerecht)
      if (fieldSize.width / fieldSize.height === maxWidthFromFieldInPx / maxHeightFromFieldInPx) ret = e2 // Snake Felt hat daselbe seitenverhältnis wie das verfügbare Feld
      else if (fieldSize.width / fieldSize.height /* ]0;1[ */ > maxWidthFromFieldInPx / maxHeightFromFieldInPx /* ]0;1[ */) ret = e1 // Snake Felt stößt zuerst links/rechts an => width = verfügbare Breite
      else ret = e5 // Snake Felt stößt zuerst oben/unten an
    } else ret = e5 // Snake Feld ist ein Rechteck (hochkant)
  }

  return ret
}

function pixelToVX (pixelX, bodyLengthInPx) {
  return pixelX / bodyLengthInPx * 100
}

function vXToPixel (vx, bodyLengthInPx) {
  return bodyLengthInPx / 100 * vx
}

function round (wert, dez) {
  wert = parseFloat(wert)
  if (!wert) return 0

  dez = parseInt(dez)
  if (!dez) dez = 0

  var umrechnungsfaktor = Math.pow(10, dez)

  return Math.round(wert * umrechnungsfaktor) / umrechnungsfaktor
}

module.exports = {
  getRandomPosition,
  pointsAreEqual,
  getVWandVH,
  pixelToVX,
  vXToPixel,
  getFieldSize,
  round
}
