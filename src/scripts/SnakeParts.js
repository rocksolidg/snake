import compose from 'lodash/fp/compose'
const MatrixField = require('./MatrixField')

const hasColor = superclass => class extends superclass {
  constructor (color, ...args) {
    super(...args)
    this.color = color
  }

  setColor (color) {
    this.color = color
  }

  getColor () {
    return this.color
  }
}
/*
const hasType = superclass => class extends superclass {
  constructor (type, ...args) {
    super(...args)
    this.type = type
  }

  getType () {
    return this.type
  }
} */

const hasChar = superclass => class extends superclass {
  constructor (char, ...args) {
    super(...args)
    this.char = char
  }

  setChar (char) {
    this.char = char
  }

  getChar () {
    return this.char
  }
}

// const hasSvg = superclass => class extends superclass {}

// const SnakePartMixin = compose(hasType)(MatrixField)
class SnakePart extends MatrixField {
  constructor (...args) {
    super(...args)
    this.head = false
    this.next = null
    this.lastPosition = null
  }

  /*
  static getSnakePartTypes () {
    return {
      monochrome: 1
    }
  }
*/
  setPosition (newPos) { // overwrite setPosition from class MatrixField
    this.lastPosition = this.position
    this.position = newPos
  }

  getLastPosition () {
    return this.lastPosition
  }

  setHead (value) {
    this.head = value
  }

  isHead () {
    return this.head
  }

  setNextPart (value) {
    this.next = value
  }

  getNextPart () {
    return this.next
  }
}

class ColorSnakePart extends compose(hasColor)(SnakePart) {}

class CharSnakePart extends compose(hasColor, hasChar)(SnakePart) {}

class Food extends compose(hasColor)(MatrixField) {}

export {
  Food,
  ColorSnakePart,
  CharSnakePart,
  SnakePart
}
