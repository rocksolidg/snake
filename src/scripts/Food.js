const MatrixField = require('./MatrixField')

class Food extends MatrixField {
  constructor (position, type) {
    super(position)
    this.type = type
    this.color = null
  }

  static getFoodTypes () {
    return {
      monochrome: 1
    }
  }

  getType () {
    return this.type
  }

  getColor () {
    return this.color
  }

  setColor (color) {
    this.color = color
  }
}

export default Food
