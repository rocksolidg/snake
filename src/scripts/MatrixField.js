class MatrixField {
  constructor (position) {
    this.position = position
  }

  getPosition () {
    return this.position
  }

  setPosition (value) {
    this.position = value
  }
}

module.exports = MatrixField
