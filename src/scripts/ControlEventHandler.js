import { State } from './../scripts/Constants'

class ControlEventHandler {
  constructor (vuexStore) {
    this.$store = vuexStore
  }

  onKeyEvent = (e) => {
    const gameState = this.$store.getters['game/getGameState']
    if (e.keyCode === 65 || e.keyCode === 97 /* a */ || e.keyCode === 37 /* left */) this.pressControlKey('left')
    if (e.keyCode === 87 || e.keyCode === 119 /* w */ || e.keyCode === 38 /* up */) this.pressControlKey('up')
    if (e.keyCode === 68 || e.keyCode === 100 /* d */ || e.keyCode === 39 /* right */) this.pressControlKey('right')
    if (e.keyCode === 83 || e.keyCode === 115 /* s */ || e.keyCode === 40 /* down */) this.pressControlKey('down')
    if (e.keyCode === 32 /* empty */ || e.keyCode === 19 /* pause/break */) {
      const activeEle = document.activeElement
      const activeElementsClassList = [...activeEle.classList]
      if (activeElementsClassList.includes('control-button') && activeElementsClassList.includes('play-pause')) activeEle.blur()

      if (gameState === State.running) this.$store.dispatch('game/setGameState', State.paused)
      else if (gameState === State.paused) this.$store.dispatch('game/setGameState', State.running)
    }
  }

  pressControlKey = (directionString) => {
    const gameState = this.$store.getters['game/getGameState']
    let checkDirection = true
    if (gameState === State.paused) {
      const startPerControlKeyAllowed = this.$store.getters['settings/control/getStartGameMethodeValue'] === 1
      const startPerControlKeyOnlyAtGameStartAllowed = this.$store.getters['settings/control/getStartGameMethodeValue'] === 2
      const isGameStarting = this.$store.getters['game/getGameStart']
      const controlNr = this.$store.getters['settings/control/getControlNrValue']
      let isControlKeyEventRelevant = true
      if (controlNr === 0 && (directionString === 'up' || directionString === 'down')) isControlKeyEventRelevant = false
      if ((startPerControlKeyAllowed || (startPerControlKeyOnlyAtGameStartAllowed && isGameStarting)) && isControlKeyEventRelevant) {
        this.$store.dispatch('game/setGameState', State.running)
        checkDirection = false
      }
    }

    this.$store.dispatch('game/changeDirectionTo', { newDirection: directionString, checkDirection })
  }
}

export default ControlEventHandler
