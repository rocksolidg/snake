
// The order of all these arrays in this file is very important for the programm
const StartGameSelection = [
  {
    name: 'Start-/Stopp- Taste/Button',
    description: 'Bei dieser Einstellung kann das Spiel durch den Startbutton über dem Spielfeld oder durch Drücken der Leertaste oder Pause-Taste gestartet und auch wieder pausiert werden. '
  },
  {
    name: 'Start-/Stopp- Taste/Button oder Steuerungstaste/Wischen',
    description: 'Bei dieser Einstellung besteht die Möglichkeit des Startens und Pausierens wie in der ersten Einstellung. Hinzu kommt die Möglichkeit, das Spiel durch Drücken einer Steuerungstaste, also den vier (bzw zwei) Pfeiltasten oder den Tasten W, A, S und D (bzw A und D) zu starten und damit direkt die Startrichtung der Schlange zu beeinflussen. Das Spiel kann auch durch Wischen gestartet werden, falls dies in der Einstellung "Steuerungsmodus" ausgewählt ist. '
  },
  {
    name: 'Start-/Stopp- Taste/Button und nur am Anfang duch Steuerungstaste',
    description: 'Bei dieser Einstellung bestehen die Möglichkeiten der zweiten Einstellung, jedoch kann nur am Anfang das Spiel durch Steuerungstasten bzw Wischen gestartet werden, nicht nachdem es pausiert wurde. '
  }
]

const SnakeAppearance = [
  {
    name: 'Einfarbig',
    description: 'Die Schlange ist einfarbig. Die Farbe kann in der Einstellung unter "Farben"->"Schlangenteil" festgelegt werden. '
  },
  {
    name: 'Regenbogen',
    description: 'Die Schlange hat Regenbogenfarben. Jedes neue Element der Schlange erhält die nächste Farbe des Regenbogens. '
  },
  {
    name: 'Text',
    description: 'Den Text, den du hier angeben kannst, ist auf der Schlange zu sehen. Jeder Teil der Schlange hat einen Buchstaben. '
  }
]

const ControlType = [
  {
    name: 'Zwei Tasten',
    description: 'Verwendete Tasten: Die zwei Pfeiltasten nach links und rechts / Tasten A und D \n' +
      'Die Schlange ändert ihre Richtung entweder aus ihrer Sicht nach links oder rechts. \n\n' +
      'Beispiel: \n' +
      'Wenn die Schlange sich nach rechts bewegt und man die Pfeiltaste nach links drückt, bewegt sich die Schlange nun nach oben.'
  },
  {
    name: 'Vier Tasten',
    description: 'Verwendete Tasten: Die vier Pfeiltasten / Tasten W, A, S und D \n' +
      'Die Schlange bewegt sich in die Richtung, die du durch die vier Pfeiltasten oder die Tasten W, A, S und D angegeben hast. Sie kann jedoch ihre Richtung nicht in die der aktuellen Bewegungsrichtung genau entgegengesetzen Richtung wechseln. \n\n' +
      'Beispiel: \n' +
      'Wenn man also den Pfeil nach oben oder die Taste W drückt, bewegt sich die Schlange nach oben, außer sie bewegte sich vorher nach unten.'
  },
  {
    name: 'Kontrollpanel',
    description: 'Die Schlange bewegt sich in die Richtung, die du durch die vier Buttons unter dem Spielfeld angegeben hast. Sie kann jedoch ihre Richtung nicht in die der aktuellen Bewegungsrichtung genau entgegengesetzten Richtung wechseln. '
  },
  {
    name: 'Wischen',
    description: 'Die Schlange bewegt sich in die Richtung in die du wischst. Du kannst sowohl mit der Maus, als auch mit den Fingern auf Touch-Bildschirmen wischen. \n\n' +
      'Beispiel: \n' +
      'Wenn du nach links wischst, bewegt sich die Schlange nach links, außer sie bewegte sich vorher nach rechts.'
  }
]

const StartDirection = [
  {
    name: 'Oben' // top
  },
  {
    name: 'Rechts' // right
  },
  {
    name: 'Unten' // bottom
  },
  {
    name: 'Links' // left
  }
]

module.exports = {
  StartGameSelection,
  SnakeAppearance,
  StartDirection,
  ControlType
}
