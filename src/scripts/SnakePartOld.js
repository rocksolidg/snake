import Classes from './SnakeParts'
const MatrixField = require('./MatrixField')

class SnakePart extends MatrixField {
  constructor (position, type) {
    super(position)
    this.head = false
    this.next = null
    this.lastPosition = null
    this.type = type
    this.color = null
  }

  static getSnakePartTypes () {
    return {
      monochrome: 1
    }
  }

  getType () {
    return this.type
  }

  getColor () {
    return this.color
  }

  setColor (color) {
    this.color = color
  }

  setPosition (newPos) { // overwrite setPosition from class MatrixField
    this.lastPosition = this.position
    this.position = newPos
  }

  getLastPosition () {
    return this.lastPosition
  }

  setHead (value) {
    this.head = value
  }

  isHead () {
    return this.head
  }

  setNextPart (value) {
    this.next = value
  }

  getNextPart () {
    return this.next
  }
}

console.log(SnakePart)
console.log(Classes.ColorSnakePart)
const o = new Classes.ColorSnakePart('#aaaaaa', { x: 2, y: 3 })
console.log(o)

export default Classes.ColorSnakePart
