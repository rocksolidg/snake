
class RainbowColorGenerator {
  constructor () {
    this.pointer = 0
    this.colors = ['#9400D3', '#4B0082', '#0000FF', '#00FF00', '#FFFF00', '#FF7F00', '#FF0000']
  }

  getNextColor () {
    const color = this.colors[this.pointer]
    if (this.pointer < (this.colors.length - 1)) this.pointer++
    else this.pointer = 0
    return color
  }

  reset () {
    this.pointer = 0
  }
}

export default new RainbowColorGenerator()
