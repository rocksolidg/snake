const State = {
  running: 1,
  paused: 2,
  stopped: 3
}

// It is important that the following object has the same key-value-pairs as the array StartDirection in SelectionSettings.js
const Direction = {
  top: 0,
  right: 1,
  bottom: 2,
  left: 3
}

const Sections = {
  start: 0,
  settings: 1,
  game: 2
}

const HelpDialogTexts = {
  game: {
    subtitle: 'Spiel',
    text: 'Ziel des Spiels: \n' +
          'Deine Schlange soll so lang wie möglich werden. \n\n' +
          'Steuerung der Schlange: \n' +
          'In den Einstellungen kannst du unter "Steuerung" bei den entsprechenden Einstellungsmöglichkeiten nachlesen, wie du die Schlange steuern kannst und wie du das Spiel pausieren und starten kannst. '
  }
}

const SettingTypes = {
  selection: 1,
  number: 2,
  boolean: 3,
  color: 4
}

module.exports = {
  State,
  Direction,
  Sections,
  HelpDialogTexts,
  SettingTypes
}
