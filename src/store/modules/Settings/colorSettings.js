export default {
  namespaced: true,
  state: () => ({ // Write Colors in hexadecimal
    page: {
      actualDesign: {
        value: 'dunkel',
        name: 'Design',
        helpText: 'Hier kannst du das verwendete Design einstellen.\n\nDas Design bezieht sich nur auf die farbliche Gestaltung außerhalb des Spielfeldes. Die Farben für das Spielfeld werden in separaten Einstellungen festgelegt.'
      },
      designs: [
        {
          name: 'dunkel',
          primaryAccentColor: '#00ff00',
          primaryFontColor: '#ffffff',
          primaryBgColor: '#333',
          secondaryBgColor: '#000000'
        },
        {
          name: 'hell',
          primaryAccentColor: '#00ff00',
          primaryFontColor: '#000000',
          primaryBgColor: '#ffffff',
          secondaryBgColor: '#cccccc'
        }
      ]
    },
    snake: {
      head: {
        value: '#0074FF',
        name: 'Schlangenkopf',
        helpText: 'Hier kannst du die Farbe des Schlangenkopfes einstellen.'
      },
      part: {
        value: '#00F9FF',
        name: 'Schlangenteil',
        helpText: 'Hier kannst du die Farbe der Schlangenteile einstellen.'
      },
      text: {
        value: '#000000',
        name: 'Schlangentext',
        helpText: 'Hier kannst du die Farbe des Textes einstellen, der auf der Schlange erscheint.'
      }
    },
    matrixField: {
      value: '#444444',
      name: 'Feldpunkte',
      helpText: 'Hier kannst du die Farbe der Feldpunkte einstellen.'
    },
    gridGap: {
      value: '#000000',
      name: 'Feldgitter',
      helpText: 'Hier kannst du die Farbe des Gitters zwischen den Feldpunkten einstellen.'
    },
    food: {
      value: '#FF0000',
      name: 'Essen',
      helpText: 'Hier kannst du die Farbe des Essens einstellen, welches die Schlange isst um länger zu werden.'
    }
  }),
  getters: {
    getSnakeHeadValue (state) { return state.snake.head.value },
    getSnakeHead (state) { return state.snake.head },
    getSnakePartValue (state) { return state.snake.part.value },
    getSnakePart (state) { return state.snake.part },
    getSnakeTextValue (state) { return state.snake.text.value },
    getSnakeText (state) { return state.snake.text },
    getMatrixFieldValue (state) { return state.matrixField.value },
    getMatrixField (state) { return state.matrixField },
    getFoodValue (state) { return state.food.value },
    getFood (state) { return state.food },
    getGridGapValue (state) { return state.gridGap.value },
    getGridGap (state) { return state.gridGap },
    getActualDesignValue (state) { return state.page.actualDesign.value },
    getActualDesign (state) { return state.page.actualDesign },
    getDesigns (state) { return state.page.designs },
    getDesign (state) {
      const ac = state.page.actualDesign.value
      const designs = state.page.designs

      return designs.find((design) => {
        if (design.name === ac) return true
      })
    }
  },
  mutations: {
    SET_SNAKE_HEAD (state, value) {
      state.snake.head.value = value
    },
    SET_SNAKE_PART (state, value) {
      state.snake.part.value = value
    },
    SET_MATRIX_FIELD (state, value) {
      state.matrixField.value = value
    },
    SET_FOOD (state, value) {
      state.food.value = value
    },
    SET_ACTUAL_DESIGN (state, value) {
      state.page.actualDesign.value = value
    },
    SET_GRID_GAP (state, value) {
      state.gridGap.value = value
    },
    SET_SNAKE_TEXT (state, value) {
      state.snake.text.value = value
    }
  },
  actions: {
    setSnakeHead ({ commit }, newSnakeHead) {
      commit('SET_SNAKE_HEAD', newSnakeHead)
    },
    setSnakePart ({ commit }, newSnakePart) {
      commit('SET_SNAKE_PART', newSnakePart)
    },
    setMatrixField ({ commit }, newMatrixField) {
      commit('SET_MATRIX_FIELD', newMatrixField)
    },
    setFood ({ commit }, newFood) {
      commit('SET_FOOD', newFood)
    },
    setGridGap ({ commit }, newGridGap) {
      commit('SET_GRID_GAP', newGridGap)
    },
    setActualDesignByName ({ commit }, name) {
      commit('SET_ACTUAL_DESIGN', name)
    },
    setSnakeText ({ commit }, newText) {
      commit('SET_SNAKE_TEXT', newText)
    }
  }
}
