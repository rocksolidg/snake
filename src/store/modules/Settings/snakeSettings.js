import { SnakeAppearance, StartDirection } from './../../../scripts/SettingSelectionConstants'
import { SettingTypes } from './../../../scripts/Constants'
import handleSetSettingValidation from './handleSetSettingValidation.js'
export default {
  namespaced: true,

  state: () => ({
    speed: {
      type: SettingTypes.number,
      value: 7,
      name: 'Geschwindigkeit',
      helpText: 'Hier kannst du die Geschwindigkeit der Schlange einstellen.',
      range: {
        min: 0.5,
        max: 60
      },
      step: 0.01
    },
    // acceleration: 0, // Not used
    growthRate: {
      type: SettingTypes.number,
      value: 1,
      name: 'Wachstumsrate',
      helpText: 'Hier kannst du die Wachstumsrate der Schlange einstellen. \n\nDie Wachstumsrate gibt an, um wieviele Teile die Schlange nach dem Essen eines Items wächst. ',
      range: {
        min: 0,
        max: 50
      },
      step: 1
    },
    beginningLength: {
      type: SettingTypes.number,
      value: 1,
      name: 'Startlänge',
      helpText: 'Hier kannst du die Startlänge der Schlange einstellen.',
      range: {
        min: 1,
        max: 50
      },
      step: 1
    },
    startDirection: {
      type: SettingTypes.selection,
      value: 3,
      name: 'Startrichtung',
      helpText: 'Hier kannst du die Startrichtung der Schlange einstellen. \n\nIn diese Richtung läuft die Schlange, wenn das Spiel nicht mit einer Steuerungstaste in eine andere Richtung gestartet wurde.',
      allValues: StartDirection
    },
    appearance: {
      type: SettingTypes.selection,
      value: 2, // index of position in array
      name: 'Erscheinungsbild',
      helpText: '',
      allValues: SnakeAppearance
    },
    text: {
      value: 'Was für ein cooles Spiel! ',
      name: 'Text',
      helpText: 'Hier kannst du den Text eingeben, welcher auf der Schlange zu sehen sein soll.'
    },
    toggleVar: true
  }),
  getters: {
    getSpeedValue (state) { return state.speed.value },
    getSpeed (state) { return state.speed },
    getGrowthRateValue (state) { return state.growthRate.value },
    getGrowthRate (state) { return state.growthRate },
    getBeginningLengthValue (state) { return state.beginningLength.value },
    getBeginningLength (state) { return state.beginningLength },
    getStartDirectionValue (state) { return state.startDirection.value },
    getStartDirection (state) { return state.startDirection },
    getAppearanceValue (state) { return state.appearance.value },
    getAppearance (state) { return state.appearance },
    getToggleVar (state) { return state.toggleVar },
    getTextValue (state) { return state.text.value },
    getText (state) { return state.text }

  },
  mutations: {
    SET_SPEED (state, value) {
      state.speed.value = value
    },
    SET_GROWTH_RATE (state, value) {
      state.growthRate.value = value
    },
    SET_BEGINNING_LENGTH (state, value) {
      state.beginningLength.value = value
    },
    SET_START_DIRECTION (state, value) {
      state.startDirection.value = value
    },
    SET_APPEARANCE (state, value) {
      state.appearance.value = value
    },
    SET_TEXT (state, value) {
      state.text.value = value
    },
    TOGGLE_VAR (state) {
      state.toggleVar = !(state.toggleVar)
    }
  },
  actions: {
    setSpeed ({ commit, getters, dispatch, rootGetters }, newSpeed) {
      handleSetSettingValidation(rootGetters, dispatch, commit, 'SET_SPEED', getters.getSpeed, newSpeed)
    },
    setGrowthRate ({ commit, getters, dispatch, rootGetters }, newGrowthRate) {
      handleSetSettingValidation(rootGetters, dispatch, commit, 'SET_GROWTH_RATE', getters.getGrowthRate, newGrowthRate)
    },
    setBeginningLength ({ commit, getters, dispatch, rootGetters }, newBeginningLength) {
      handleSetSettingValidation(rootGetters, dispatch, commit, 'SET_BEGINNING_LENGTH', getters.getBeginningLength, newBeginningLength)
    },
    setStartDirection ({ commit, getters, dispatch, rootGetters }, newDirection) {
      handleSetSettingValidation(rootGetters, dispatch, commit, 'SET_START_DIRECTION', getters.getStartDirection, newDirection)
    },
    setAppearance ({ commit, getters, dispatch, rootGetters }, newAppearance) {
      handleSetSettingValidation(rootGetters, dispatch, commit, 'SET_APPEARANCE', getters.getAppearance, newAppearance)
    },
    setText ({ commit }, newText) {
      commit('SET_TEXT', newText)
    },
    toggleVar ({ commit }) {
      commit('TOGGLE_VAR')
    }
  }
}
