import { StartGameSelection, ControlType } from './../../../scripts/SettingSelectionConstants'
import { SettingTypes } from './../../../scripts/Constants'
import handleSetSettingValidation from './handleSetSettingValidation.js'
export default {
  namespaced: true,
  state: () => ({
    controlNr: {
      type: SettingTypes.selection,
      value: 1,
      name: 'Steuerungsmodus',
      helpText: 'Hier kannst du den verwendeten Steuerungsmodus einstellen. ',
      allValues: ControlType
    },
    startGame: {
      type: SettingTypes.selection,
      value: 2,
      name: 'Spiel-Starten-Methode',
      helpText: 'Hier kannst du die Methode einstellen, wie das Spiel gestartet wird. ',
      allValues: StartGameSelection
    }
  }),
  getters: {
    getControlNrValue (state) { return state.controlNr.value },
    getControlNr (state) { return state.controlNr },
    getStartGameMethodeValue (state) { return state.startGame.value },
    getStartGameMethode (state) { return state.startGame }
  },
  mutations: {
    SET_CONTROL_NR (state, value) {
      state.controlNr.value = value
    },
    SET_START_GAME_METHODE (state, value) {
      state.startGame.value = value
    }
  },
  actions: {
    setControlNr ({ commit, getters, dispatch, rootGetters }, newControlNr) {
      handleSetSettingValidation(rootGetters, dispatch, commit, 'SET_CONTROL_NR', getters.getControlNr, newControlNr)
    },
    setStartGameMethode ({ commit, getters, dispatch, rootGetters }, newMethode) {
      handleSetSettingValidation(rootGetters, dispatch, commit, 'SET_START_GAME_METHODE', getters.getStartGameMethode, newMethode)
    }
  }
}
