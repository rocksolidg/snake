module.exports = function (rootGetters, dispatch, commit, commitString, setting, value) {
  if (rootGetters['settings/isValueValid'](value, setting)) {
    commit(commitString, value)
    dispatch('settings/validSetting', setting, { root: true })
  } else dispatch('settings/invalidSetting', setting, { root: true })
}
