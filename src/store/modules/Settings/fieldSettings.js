import handleSetSettingValidation from './handleSetSettingValidation.js'
import { SettingTypes } from './../../../scripts/Constants'
export default {
  namespaced: true,

  state: () => ({
    size: {
      width: {
        type: SettingTypes.number,
        value: 20,
        name: 'Breite',
        helpText: 'Hier kannst du die Breite des Feldes einstellen. \n\n' +
                  'Die Zahl gibt an, wie viele Feldpunkte das Feld breit sein soll.',
        range: {
          min: 1,
          max: 100
        },
        step: 1
      },
      height: {
        type: SettingTypes.number,
        value: 20,
        name: 'Höhe',
        helpText: 'Hier kannst du die Höhe des Feldes einstellen. \n\n' +
        'Die Zahl gibt an, wie viele Feldpunkte das Feld hoch sein soll.',
        range: {
          min: 1,
          max: 100
        },
        step: 1
      }
    },
    gridGap: {
      type: SettingTypes.number,
      value: 10,
      name: 'Gitterbreite',
      helpText: 'Hier kannst du die Gitterbreite des Feldes, also den Abstand zwischen den Feldpunkten einstellen.',
      range: {
        min: 0,
        max: 45
      },
      step: 0.01
    },
    infiniteField: {
      type: SettingTypes.boolean,
      value: true,
      name: 'Unendliches Feld',
      helpText: 'Hier kannst du einstellen, ob das Feld "unendlich" sein soll.\n\n Wenn die Checkbox ausgewählt ist, kommt die Schlange, wenn sie aus dem Feld heraus läuft, auf der gegenüberliegenden Seite wieder ins Feld rein. Wenn die Checkbox nicht ausgewählt ist, verliert man wenn man gegen den Rand läuft.'
    }
  }),
  getters: {
    getSizeValue (state) {
      return { width: state.size.width.value, height: state.size.height.value }
    },
    getSize (state) { return state.size },
    getWidthValue (state) { return state.size.width.value },
    getWidth (state) { return state.size.width },
    getHeightValue (state) { return state.size.height.value },
    getHeight (state) { return state.size.height },
    getGridGapValue (state) { return state.gridGap.value },
    getGridGap (state) { return state.gridGap },
    getInfiniteFieldValue (state) { return state.infiniteField.value },
    getInfiniteField (state) { return state.infiniteField },

    getNumberOfMatrixFields (state) {
      return state.size.width.value * state.size.height.value
    }
  },
  mutations: {
    SET_WIDTH (state, value) {
      state.size.width.value = value
    },
    SET_HEIGHT (state, value) {
      state.size.height.value = value
    },
    SET_GRID_GAP (state, value) {
      state.gridGap.value = value
    },
    SET_INFINITE_FIELD (state, value) {
      state.infiniteField.value = value
    }
  },
  actions: {
    setWidth ({ commit, getters, dispatch, rootGetters }, newWidth) {
      handleSetSettingValidation(rootGetters, dispatch, commit, 'SET_WIDTH', getters.getWidth, newWidth)
    },
    setHeight ({ commit, getters, dispatch, rootGetters }, newHeight) {
      handleSetSettingValidation(rootGetters, dispatch, commit, 'SET_HEIGHT', getters.getHeight, newHeight)
    },
    setGridGap ({ commit, getters, dispatch, rootGetters }, gridGap) {
      handleSetSettingValidation(rootGetters, dispatch, commit, 'SET_GRID_GAP', getters.getGridGap, gridGap)
    },
    setInfiniteField ({ commit, getters, dispatch, rootGetters }, value) {
      handleSetSettingValidation(rootGetters, dispatch, commit, 'SET_INFINITE_FIELD', getters.getInfiniteField, value)
    }
  }
}
