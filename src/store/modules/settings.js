import colors from './Settings/colorSettings'
import field from './Settings/fieldSettings'
import snake from './Settings/snakeSettings'
import control from './Settings/controlSettings'
import { round } from './../../scripts/functions'
import { SettingTypes } from '../../scripts/Constants'
export default {
  namespaced: true,
  modules: {
    colors,
    field,
    snake,
    control
  },
  state: () => ({
    invalidSettings: []
  }),
  getters: {
    isValueValid (state) {
      return (value, getter) => {
        if (getter.type !== SettingTypes.number) return true
        if (value == null) return false
        let min = null
        let max = null
        let maxPassed = null
        let minPassed = null
        let stepPassed = null
        if (!getter.range) {
          maxPassed = true
          minPassed = true
        } else {
          min = getter.range.min
          max = getter.range.max
          if (min == null) minPassed = true
          if (max == null) maxPassed = true
          if (minPassed == null) minPassed = value >= min
          if (maxPassed == null) maxPassed = value <= max
        }
        const step = getter.step
        if (step == null) stepPassed = true
        else stepPassed = Number.isSafeInteger(round(value / step, 10))
        return maxPassed && minPassed && stepPassed
      }
    },
    getInvalidSettings (state) {
      return state.invalidSettings
    },
    getNumberOfCharsFromLagestNameInSetting (state, getters) {
      return (nameOfSetting) => {
        let largestNumberOfChars = 0
        const getterKeys = Object.keys(getters)
        getterKeys.forEach((key) => {
          if (key.startsWith(nameOfSetting) && getters[key] instanceof Object) {
            const settingObj = getters[key]
            if (Object.keys(settingObj).includes('name')) {
              const numberOfChars = settingObj.name.length
              if (numberOfChars > largestNumberOfChars) largestNumberOfChars = numberOfChars
            }
          }
        })
        return largestNumberOfChars
      }
    }
  },
  mutations: {
    ADD_TO_INVALID_SETTINGS (state, setting) {
      if (state.invalidSettings.map((e) => e.name).indexOf(setting.name) < 0) state.invalidSettings.push(setting)
    },
    REMOVE_FROM_INVALID_SETTINGS (state, setting) {
      const index = state.invalidSettings.map((e) => e.name).indexOf(setting.name)
      if (index < 0) return
      state.invalidSettings.splice(index, 1)
    },
    RESET_INVALID_SETTINGS (state) {
      state.invalidSettings = []
    }
  },
  actions: {
    invalidSetting ({ commit }, setting) {
      commit('ADD_TO_INVALID_SETTINGS', setting)
    },
    validSetting ({ commit }, setting) {
      commit('REMOVE_FROM_INVALID_SETTINGS', setting)
    },
    clearInvalidSettings ({ commit }) {
      commit('RESET_INVALID_SETTINGS')
    }
  }
}
