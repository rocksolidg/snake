export default {
  namespaced: true,
  state: () => ({
    open: false,
    text: 'Dialog-Text',
    title: {
      mainTitle: 'Hilfe',
      subTitle: 'Game'
    }
  }),
  getters: {
    getText (state) {
      return state.text
    },
    isOpen (state) {
      return state.open
    },
    getMainTitle (state) {
      return state.title.mainTitle
    },
    getSubTitle (state) {
      return state.title.subTitle
    }
  },
  mutations: {
    SET_OPEN (state, value) {
      state.open = value
    },
    SET_TEXT (state, value) {
      state.text = value
    },
    SET_SUBTITLE (state, value) {
      state.title.subTitle = value
    },
    SET_MAINTITLE (state, value) {
      state.title.mainTitle = value
    }
  },
  actions: {
    setOpen ({ commit }, value) {
      commit('SET_OPEN', value)
    },
    setText ({ commit }, value) {
      commit('SET_TEXT', value)
    },
    setSubTitle ({ commit }, value) {
      commit('SET_SUBTITLE', value)
    },
    setMainTitle ({ commit }, value) {
      commit('SET_MAINTITLE', value)
    }
  }
}
