/* eslint-disable no-fallthrough */
import Vue from 'vue'
import { Direction, State } from './../../scripts/Constants'
import { SnakePart, ColorSnakePart, CharSnakePart } from '../../scripts/SnakeParts'
import MatrixField from './../../scripts/MatrixField'
import Food from './../../scripts/Food'
import { getRandomPosition, pointsAreEqual } from './../../scripts/functions'
import RainbowColorGenerator from './../../scripts/RainbowcolorGenerator'
export default {
  namespaced: true,
  state: () => ({
    game_state: State.stopped,
    gameStart: false,
    matrix: [],
    direction: {
      actualDirection: null,
      plannedDirection: null
    },
    snake: {
      head: null,
      addingCount: 0,
      length: 0,
      textCurser: 0
    }
  }),
  getters: {
    getSnakeHead (state) { return state.snake.head },
    getSnake (state) { return state.snake },
    getDirection (state) { return state.direction },
    getMatrix (state) { return state.matrix },
    getMatrixHelper (state) { return state.matrixHelper },
    getGameState (state) { return state.game_state },
    getGameStart (state) { return state.gameStart },
    isEachMatrixFieldsBeingUsedFromAnSnakePart (state) {
      return (exceptPositions = []) => {
        const matrix = state.matrix
        const size = { width: matrix[0].length, height: matrix.length }
        for (let i = 0; i < size.height; i++) {
          for (let j = 0; j < size.width; j++) {
            const pixel = matrix[i][j]
            const pixelPosition = pixel.getPosition()
            const pixelsPositionInExceptPositions = exceptPositions.find((exceptPosition) => pointsAreEqual(exceptPosition, pixelPosition)) !== undefined
            if (pixelsPositionInExceptPositions) continue
            if (!(pixel instanceof SnakePart)) return false
          }
        }
        return true
      }
    }
  },
  mutations: {
    SET_MATRIX (state, matrix) {
      state.matrix = matrix
    },
    SET_SNAKE_HEAD (state, newHead) {
      state.snake.head = newHead
    },
    SET_MATRIX_PIXEL (state, matrixField) {
      Vue.set(state.matrix[matrixField.getPosition().y], matrixField.getPosition().x, matrixField)
    },
    SET_PLANNED_DIRECTION (state, dir) {
      state.direction.plannedDirection = dir
    },
    SET_ACTUAL_DIRECTION (state, dir) {
      state.direction.actualDirection = dir
    },
    INCREASE_ADDING_COUNT (state, amount = 1) {
      state.snake.addingCount += amount
    },
    DECREASE_ADDING_COUNT (state) {
      state.snake.addingCount -= 1
    },
    INCREASE_SNAKE_LENGTH (state) {
      state.snake.length += 1
    },
    SET_GAME_STATE (state, newState) {
      state.game_state = newState
    },
    RESET_ADDING_COUNT (state) {
      state.snake.addingCount = 0
    },
    RESET_SNAKE_LENGTH (state) {
      state.snake.length = 0
    },
    SET_GAME_START (state, value) {
      state.gameStart = value
    },
    MOVE_SNAKE_TEXT_CURSER (state, text) {
      if (state.snake.textCurser < text.length - 1) state.snake.textCurser++
      else state.snake.textCurser = 0
    },
    RESET_SNAKE_TEXT_CURSER (state) {
      state.snake.textCurser = 0
    }
  },
  actions: {
    reset ({ commit }) {
      commit('SET_GAME_STATE', State.stopped)
      commit('SET_MATRIX', [])
      commit('SET_PLANNED_DIRECTION', [])
      commit('SET_ACTUAL_DIRECTION', [])
      commit('SET_SNAKE_HEAD', null)
      commit('RESET_ADDING_COUNT')
      commit('RESET_SNAKE_LENGTH')
      commit('RESET_SNAKE_TEXT_CURSER')
      RainbowColorGenerator.reset()
    },
    setGameState ({ commit, getters }, s) {
      if (getters.getGameState === State.stopped && s !== State.stopped) commit('SET_GAME_START', true)
      else commit('SET_GAME_START', false)
      commit('SET_GAME_STATE', s)
    },
    startGame ({ dispatch, rootGetters }) {
      dispatch('reset')
      dispatch('setGameState', State.paused)

      dispatch('createMatrix')
      dispatch('createSnakeHeadStartpoint')
      dispatch('addToSnake', rootGetters['settings/snake/getBeginningLengthValue'] - 1)
      dispatch('createFood')
    },
    endGame ({ dispatch }) { // triggered by user who restart or cancel the game through buttons
      dispatch('setGameState', State.stopped)
      dispatch('reset')
    },
    finishGame ({ dispatch, getters }) { // triggered by user who lose or win the game through snake movements
      dispatch('setGameState', State.stopped)
      const finalLength = getters.getSnake.length

      let win = null
      if (getters.isEachMatrixFieldsBeingUsedFromAnSnakePart()) win = true
      else win = false

      const winDialog = {
        subtitle: 'Gewonnen',
        text: 'Herzlichen Glückwunsch! Du hast es geschafft das ganze Feld mit der Schlange auszufüllen und damit eine maximale Länge von ' + finalLength + ' erreicht!'
      }
      const losingDialog = {
        subtitle: 'Verloren',
        text: 'Deine Schlange hat eine Länge von ' + finalLength + ' erreicht.'
      }
      let finalDialog = losingDialog
      if (win) finalDialog = winDialog

      dispatch('dialog/setMainTitle', 'Spiel vorbei', { root: true })
      dispatch('dialog/setSubTitle', finalDialog.subtitle, { root: true })
      dispatch('dialog/setText', finalDialog.text, { root: true })
      dispatch('dialog/setOpen', true, { root: true })
    },
    createMatrix ({ commit, rootGetters }) {
      const size = rootGetters['settings/field/getSizeValue']
      const matrix = []
      for (let i = 0; i < size.height; i++) {
        const array = []
        for (let j = 0; j < size.width; j++) {
          array.push(new MatrixField({ y: i, x: j }))
        }
        matrix.push(array)
      }
      commit('SET_MATRIX', matrix)
    },
    createSnakeHeadStartpoint ({ commit, dispatch, rootGetters }) {
      const startDir = rootGetters['settings/snake/getStartDirectionValue']
      commit('SET_ACTUAL_DIRECTION', startDir)
      commit('SET_PLANNED_DIRECTION', startDir)
      const randPos = getRandomPosition(rootGetters['settings/field/getSizeValue'])
      const newHead = new ColorSnakePart(rootGetters['settings/colors/getSnakeHeadValue'], randPos)
      dispatch('turnSnakePartToHead', newHead)
      commit('INCREASE_SNAKE_LENGTH')
      commit('SET_MATRIX_PIXEL', newHead)
    },
    createFood ({ commit, getters, rootGetters }, impossiblePositions = []) { // effizienter machen
      let randPos = null
      let matrixFieldOnRandPos = null

      do {
        if (getters.isEachMatrixFieldsBeingUsedFromAnSnakePart(impossiblePositions)) return
        randPos = getRandomPosition(rootGetters['settings/field/getSizeValue'])
        matrixFieldOnRandPos = getters.getMatrix[randPos.y][randPos.x]
      }
      while (matrixFieldOnRandPos instanceof SnakePart || impossiblePositions.find((impossiblePosition) => pointsAreEqual(impossiblePosition, randPos)) !== undefined)

      const color = rootGetters['settings/colors/getFoodValue']
      const food = new Food(randPos, Food.getFoodTypes().monochrome)
      food.setColor(color)

      commit('SET_MATRIX_PIXEL', food)
    },
    turnSnakePartToHead ({ commit }, snakePart) {
      snakePart.setHead(true)
      commit('SET_SNAKE_HEAD', snakePart)
    },
    addToSnake ({ commit, getters, rootGetters }, amount = 1) {
      const numberOfMatrixPixels = rootGetters['settings/field/getNumberOfMatrixFields']
      if (getters.getSnake.length + amount > numberOfMatrixPixels) amount = numberOfMatrixPixels - getters.getSnake.length
      commit('INCREASE_ADDING_COUNT', amount)
    },
    moveSnake ({ dispatch, getters, commit, rootGetters }) {
      if (!(getters.getGameState === State.running)) return
      const snakeHead = getters.getSnakeHead
      const plannedDir = getters.getDirection.plannedDirection
      commit('SET_ACTUAL_DIRECTION', plannedDir)

      dispatch('movePixelToDirection', { snakePart: snakeHead, direction: plannedDir })

      let current = snakeHead.getNextPart()
      let previous = snakeHead

      while (current != null) {
        if (!(getters.getGameState === State.running)) return
        dispatch('movePixelToPosition', { snakePart: current, newPosition: previous.getLastPosition() })
        previous = current
        current = current.getNextPart()
      }

      const lastPart = previous

      if (getters.getSnake.addingCount !== 0) {
        let newPart = null
        const snakeAppearance = rootGetters['settings/snake/getAppearanceValue']
        const color = rootGetters['settings/colors/getSnakePartValue']
        if (snakeAppearance === 0) {
          newPart = new ColorSnakePart(color, lastPart.getLastPosition())
        } else if (snakeAppearance === 1) {
          newPart = new ColorSnakePart(RainbowColorGenerator.getNextColor(), lastPart.getLastPosition())
        } else if (snakeAppearance === 2) {
          const text = rootGetters['settings/snake/getTextValue']
          const index = getters.getSnake.textCurser
          newPart = new CharSnakePart(color, text.charAt(index), lastPart.getLastPosition())
          commit('MOVE_SNAKE_TEXT_CURSER', text)
        }

        lastPart.setNextPart(newPart)
        commit('SET_MATRIX_PIXEL', newPart)
        commit('INCREASE_SNAKE_LENGTH')
        commit('DECREASE_ADDING_COUNT')
      }
    },
    movePixelToDirection ({ getters, dispatch, rootGetters }, { snakePart, direction }) {
      if (direction == null || snakePart == null) return
      const point = snakePart.getPosition()
      const newPoint = { ...point }

      const size = rootGetters['settings/field/getSizeValue']
      const isFieldInfinite = rootGetters['settings/field/getInfiniteFieldValue']

      // not used for performance reasons
      /*
      if (direction === Direction.top) {
        if (newPoint.y > 0) newPoint.y -= 1
        else if (isFieldInfinite) newPoint.y = size.height - 1
        else return dispatch('finishGame')
      } else if (direction === Direction.bottom) {
        if (newPoint.y < size.height - 1) newPoint.y += 1
        else if (isFieldInfinite) newPoint.y = 0
        else return dispatch('finishGame')
      } else if (direction === Direction.right) {
        if (newPoint.x < size.width - 1) newPoint.x += 1
        else if (isFieldInfinite) newPoint.x = 0
        else return dispatch('finishGame')
      } else if (direction === Direction.left) {
        if (newPoint.x > 0) newPoint.x -= 1
        else if (isFieldInfinite) newPoint.x = size.width - 1
        else return dispatch('finishGame')
      }
      */
      const condition1 = newPoint.y > 0
      const condition2 = newPoint.y < size.height - 1
      const condition3 = newPoint.x < size.width - 1
      const condition4 = newPoint.x > 0
      switch (direction) {
        case Direction.top:
          if (condition1) newPoint.y -= 1
          else if (isFieldInfinite) newPoint.y = size.height - 1
          if (condition1 || isFieldInfinite) break
        case Direction.bottom:
          if (condition2) newPoint.y += 1
          else if (isFieldInfinite) newPoint.y = 0
          if (condition2 || isFieldInfinite) break
        case Direction.right:
          if (condition3) newPoint.x += 1
          else if (isFieldInfinite) newPoint.x = 0
          if (condition3 || isFieldInfinite) break
        case Direction.left:
          if (condition4) newPoint.x -= 1
          else if (isFieldInfinite) newPoint.x = size.width - 1
          if (condition4 || isFieldInfinite) break
        default:
          return dispatch('finishGame')
      }

      const fieldofNewPointInMatrix = getters.getMatrix[newPoint.y][newPoint.x]
      if (fieldofNewPointInMatrix instanceof Food) {
        dispatch('addToSnake', rootGetters['settings/snake/getGrowthRateValue'])
        dispatch('createFood', [newPoint])
      } else if (fieldofNewPointInMatrix instanceof SnakePart) {
        return dispatch('finishGame')
      }

      dispatch('movePixelToPosition', { snakePart, newPosition: newPoint })
    },
    movePixelToPosition ({ commit }, { snakePart, newPosition }) {
      if (newPosition == null || snakePart == null) return

      snakePart.setPosition(newPosition)

      commit('SET_MATRIX_PIXEL', new MatrixField(snakePart.getLastPosition()))
      commit('SET_MATRIX_PIXEL', snakePart)
    },
    changeDirectionTo ({ commit, getters, rootGetters }, { newDirection, checkDirection = true }) {
      if (!(getters.getGameState === State.running)) return
      const controlNr = rootGetters['settings/control/getControlNrValue']

      let acDir = getters.getDirection.actualDirection
      if (controlNr === 0) { // Hier abfangen das man mehrmals vor der nächsten bewegung die geplante richtung verändern kann sodass die geplante richtung in die gegenrichtung zeigt
        if (newDirection === 'left' && acDir !== 1) acDir -= 1
        else if (newDirection === 'left' && acDir === 1) acDir = 4
        else if (newDirection === 'right' && acDir !== 4) acDir += 1
        else if (newDirection === 'right' && acDir === 4) acDir = 1
      } else {
        if (checkDirection) {
          if (acDir === Direction.top && newDirection === 'down') return
          if (acDir === Direction.bottom && newDirection === 'up') return
          if (acDir === Direction.right && newDirection === 'left') return
          if (acDir === Direction.left && newDirection === 'right') return
        }

        if (newDirection === 'left') acDir = Direction.left
        else if (newDirection === 'right') acDir = Direction.right
        else if (newDirection === 'up') acDir = Direction.top
        else if (newDirection === 'down') acDir = Direction.bottom
      }
      commit('SET_PLANNED_DIRECTION', acDir)
    }
  }
}
