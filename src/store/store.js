import Vue from 'vue'
import Vuex from 'vuex'
import game from './modules/game'
import settings from './modules/settings'
import dialog from './modules/dialog'
import { Sections } from './../scripts/Constants'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    game,
    settings,
    dialog
  },
  state: {
    mainSection: Sections.start
  },
  mutations: {
    SET_MAIN_SECTION (state, value) {
      state.mainSection = value
    }
  },
  getters: {
    getMainSection (state) {
      return state.mainSection
    }
  },
  actions: {
    setMainSection ({ commit }, newSection) {
      commit('SET_MAIN_SECTION', newSection)
    }
  }
})

export default store
