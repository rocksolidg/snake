import Vue from 'vue'
import App from './App.vue'
import Store from './store/store'
import { VueHammer } from 'vue2-hammer'

// eslint-disable-next-line no-unused-vars
import globalComponentsIgnored from './components/_globals.js'

// Info: https://hammerjs.github.io/api/
Vue.use(VueHammer)

Vue.config.productionTip = false

new Vue({
  store: Store,
  render: h => h(App)
}).$mount('#app')
