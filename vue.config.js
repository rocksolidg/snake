module.exports = {
  publicPath: process.env.NODE_ENV === 'gitlab'
    ? '/snake/'
    : '/'
}
